package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.synth.SynthSeparatorUI;

import DateinSchreiber.Promocode_in_datei;
import logic.IPromoCodeLogic;
import java.awt.Color;
import javax.swing.JTextPane;
import java.awt.Panel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.JTextField;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	
	private JLabel lblPromocode;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private Panel panel;
	private Panel panel_Button;
	private JPanel contentPane;
	private JPanel panel_1;
	private JPanel PanelCenter;
	private JPanel panel_2;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 235);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		lblPromocode = new JLabel("");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		contentPane.add(lblPromocode);
		
		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblBeschriftungPromocode = new JLabel("Promocode generator");
		panel_1.add(lblBeschriftungPromocode);
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblBeschriftungPromocode.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 19));
		lblBeschriftungPromocode.setBackground(Color.GREEN);
		
		panel_Button = new Panel();
		contentPane.add(panel_Button, BorderLayout.SOUTH);
		panel_Button.setLayout(new BorderLayout(0, 0));
		
		JButton btnProgrammBeenden_1 = new JButton("Programm Beenden");
		panel_Button.add(btnProgrammBeenden_1, BorderLayout.SOUTH);
		btnProgrammBeenden_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Programm_Beenden();
			}
		});
		
		JButton btnNewPromoCode = new JButton("Generiere neuen Promotioncode");
		panel_Button.add(btnNewPromoCode, BorderLayout.NORTH);
		
		PanelCenter = new JPanel();
		contentPane.add(PanelCenter, BorderLayout.CENTER);
		PanelCenter.setLayout(new BorderLayout(0, 0));
		
		panel = new Panel();
		PanelCenter.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		lblNewLabel = new JLabel("Promotioncode");
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("fuer 2-fach IT$");
		panel.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("in einer Stunde:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel_2);
		
		panel_2 = new JPanel();
		PanelCenter.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		textField = new JTextField();
		textField.setEditable(false);
		panel_2.add(textField);
		textField.setColumns(4);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		panel_2.add(textField_1);
		textField_1.setColumns(4);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		panel_2.add(textField_2);
		textField_2.setColumns(4);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		panel_2.add(textField_3);
		textField_3.setColumns(4);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		panel_2.add(textField_4);
		textField_4.setColumns(4);
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] code = new int[logic.generatePromoCode().length];
				code = logic.generatePromoCode();
				textField.setText(""+code[0]);
				textField_1.setText(""+code[1]);
				textField_2.setText(""+code[2]);
				textField_3.setText(""+code[3]);
				textField_4.setText(""+code[4]);
				String number = code.toString();
				
				try {
					Promocode_in_datei.test(number);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
	 
		});
		this.setVisible(true);
		
	}
	public String lessbarerPromocode(IPromoCodeLogic logic) {
		String number ="";
		for(int i = 0; i < logic.generatePromoCode().length;i++) {
			number += logic.generatePromoCode()[i];
			if(i != logic.generatePromoCode().length-1) {
				number += "-";
			}
			
		}
		return number;
	}
	
	public void Programm_Beenden()	{
		//this.dispose();
		System.exit(0);
	}
}
