package start;

import gui.PromocodeGUI;
import gui.PromocodeTUI;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogic;

import java.io.IOException;

import DateinSchreiber.Promocode_in_datei;
import data.IPromoCodeData;
import data.PromoCodeData;

public class PromoCodeStart {

	public static void main(String[] args) throws IOException {
		IPromoCodeData data = new PromoCodeData();
		IPromoCodeLogic logic = new PromoCodeLogic(data);
		new Promocode_in_datei();
		new PromocodeGUI(logic);
		new PromocodeTUI(logic);
		
	
	}

}
