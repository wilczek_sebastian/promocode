/**
 * 
 */
package logic;

import java.util.Random;

import data.IPromoCodeData;

/**
 * @author Tim Tenbusch
 * @version 1.0 vom 07.10.2015
 */
public class PromoCodeLogic implements IPromoCodeLogic {
	
	int[] promocodeA = new int[5];
	boolean counter = false;
	IPromoCodeData data;
	
	public PromoCodeLogic(IPromoCodeData data) {
		this.data = data;
	}

	@Override
	public String getNewPromoCode() {
		return tryToValidatePromoCode(insertHyphen(generatePromoCode()));
	}

	private String tryToValidatePromoCode(String code) {
		final int MAXVERSUCHE = 3;
		int versuche = 0;
		do {
			if (savePromoCode(code))
				return code;
			else
				versuche++;
		} while (versuche < MAXVERSUCHE);
		return "Leider ist ein Fehler aufgetreten :(";
	}

	public int[] generatePromoCode() {
		Random rand = new Random();
		for(int i = 0;i <promocodeA.length;i++) {
			do {
				int number = rand.nextInt(99999);
				if(number >= 10000) {
					promocodeA[i] = number;
					counter = true;
				}
			}while(counter == false);
		}
		return promocodeA;
	}

	/**
	 * f�gt Bindestriche alle 5 Buchstaben ein
	 */
	public String insertHyphen(int[] js) {
		StringBuffer newCode = new StringBuffer();
		for (int i = 0; (i + 4) < js.length; i += 5) {
			newCode.append(js.length);
			newCode.append('-');
		}
		newCode.deleteCharAt(newCode.length() - 1);
		return newCode.toString();
	}

	@Override
	public boolean savePromoCode(String code) {
		// Promocode schon vorhanden
		if (data.isPromoCode(code))
			return false;
		// speichere Promocode in Datenhaltung
		return data.savePromoCode(code);
	}

}
