package logic;

public class PromoCodeLogicStub implements IPromoCodeLogic {

	@Override
	public String getNewPromoCode() {
		return "THERE-IS-NO-GOLD-FOR-YOU";
	}

	@Override
	public int[] generatePromoCode() {
		return null;
	}

	@Override
	public boolean savePromoCode(String code) {
		return false;
	}

}
